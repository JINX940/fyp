import React from "react";
import mermaid from "mermaid"

import "./Style.css"

class Basic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            graphDefinition: `graph TB
            1(Start)-->2[abc]
            2[abc]-->3[asd]`
        }
        this.addgraph = this.addgraph.bind(this)
    }

    componentDidMount() {
        mermaid.initialize({ startOnLoad: true })
        var graph = document.getElementById('graph');
        mermaid.render('theGraph', this.state.graphDefinition, function (svgCode) {
            graph.innerHTML = svgCode
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.graphDefinition !== prevState.graphDefinition) {
            var graph = document.getElementById('graph');
            mermaid.render('theGraph', this.state.graphDefinition, function (svgCode) {
                graph.innerHTML = svgCode
            })
        }
    }

    addgraph() {
        this.setState({
            graphDefinition: this.state.graphDefinition + "\n2-->3"
        })
    }
    minus(){

    }
    
    render() {
        return (
            <div>
                <div id="graph" />
                <button onClick={this.addgraph}>add</button>
                <button onClick={this.minus}>delete</button>
            </div>

        );
    }
}

export default Basic;