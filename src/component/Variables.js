import React from "react";
import "./Style.css"

class Variables extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            fields: {
                x: '',
                y: '',
                z: '',
                i: ''
            },
            regexp: /^[0-9\b]+$/
        }
    }

    style = {
        backgroundColor: "rgb(195, 227, 223)",
    }

    componentDidMount() {
        let fields = this.state.fields; // do this for the first time input value
        for (var index = 0; index < this.props.Variables.variable.length; index++) {
            fields[this.props.Variables.variable[index].name] = this.props.Variables.variable[index].startingValue[0];//first value of variable
        }
        this.setState({ fields }, () => {
            this.sendFieldsToParent();
        });
    }

    renderTableData() {
        return this.props.Variables.variable.map((variable, index) => {
            var { name, type, value, stepUntil, start } = variable
            var addStyle = false
            var x

            for (var i = 0; i < this.props.stepUntil[name].length; i++) {
                console.log(this.props.stepUntil[name][i])
                if (this.props.stepUntil[name][i] >= this.props.step && this.props.line >= start) {
                    var x = i
                    if (this.props.stepUntil[name][i] === this.props.step || start === this.props.step) {
                        addStyle = true
                    }
                    for (var key in this.props.styleValue) {
                        if (key === name) {
                            this.props.styleValue[key].forEach(value => {
                                if (this.props.step === value) {
                                    addStyle = true
                                }
                            });
                        }
                    }

                    return (
                        <tr key={index}>
                            <td>{name}</td>
                            <td>{type}</td>
                            <td style={addStyle ? this.style : null}>{this.props.value[name][x]}</td>
                        </tr>
                    )
                }
            }
        })
    }

    sendFieldsToParent() {
        this.props.sendFields(this.state.fields);
    }

    handleIntChange(field, e) {
        let fields = this.state.fields;
        if (this.state.regexp.test(e.target.value)) {
            fields[field] = e.target.value;
            this.setState({ fields }, () => {
                this.sendFieldsToParent();
            });
        }
    }

    handleBooleanChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields }, () => {
            this.sendFieldsToParent();
        });
    }

    renderTableDataInput() {
        return this.props.Variables.variable.map((variable, index) => {
            var { name, type, value, stepUntil, start } = variable
            var addStyle = false
            var x
            if (type === "Integer") {
                return (
                    <tr key={index}>
                        <td>{name}</td>
                        <td>{type}</td>
                        <td style={addStyle ? this.style : null}>{<input type="number"
                            onChange={this.handleIntChange.bind(this, name)}
                            value={this.state.fields[name]} />}</td>
                    </tr>
                )
            } else if (type === "Boolean") {
                return (
                    <tr key={index}>
                        <td>{name}</td>
                        <td>{type}</td>
                        <td style={addStyle ? this.style : null}>{<select
                            onChange={this.handleBooleanChange.bind(this, name)}
                            value={this.state.fields[name]} >
                            <option value="true">True</option>
                            <option value="false">False</option>
                        </select>}</td>
                    </tr>
                )
            }
        })
    }

    render() {
        let table
        if (this.props.step === -1 || this.props.step === "-1") {
            table = this.renderTableDataInput()
        } else {
            table = this.renderTableData()
        }

        return (
            <div className='children2'>
                <table id='table'>
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Value</th>
                        </tr>
                    </tbody>
                    <tbody>
                        {table}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Variables;