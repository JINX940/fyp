import React from "react";

import "./Style.css"

class ControlCode extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            line: -1,
            step: -1,
            playDisabled: false,
            pauseDisabled: true,
            nextDisabled: false,
            reverseDisabled: true,
            sliderDisabled: false,
            speed: 3000
        }
        this.timerId = 0;
        this.playFunction = this.playFunction.bind(this);
        this.stopFunction = this.stopFunction.bind(this);
        this.pauseFunction = this.pauseFunction.bind(this);
        this.reverseFunction = this.reverseFunction.bind(this);
        this.nextFunction = this.nextFunction.bind(this);
    }
    liStyle = {
        backgroundColor: "rgb(44, 191, 173)",
        paddingLeft: "2em",
    }
    before = {
        backgroundColor: "rgb(209, 222, 221)",
    }

    playFunction() {
        this.setState({
            pauseDisabled: false,
            playDisabled: true,
            nextDisabled: true,
            reverseDisabled: true,
            sliderDisabled: true
        });
        this.timerId = setInterval(() => {
            this.setState({ step: this.state.step + 1 });
        }, this.state.speed);
    }

    stopFunction() {
        this.setState({
            step: -1,
            line: -1,
            pauseDisabled: true,
            playDisabled: false,
            nextDisabled: false,
            show: false,
            loop: 0,
            next: true,
            reverseDisabled: true,
            sliderDisabled: false
        })
        clearInterval(this.timerId);
    }

    pauseFunction() {
        if (this.state.step === -1) {
            this.setState({
                step: -1,
                playDisabled: false,
                pauseDisabled: true,
                nextDisabled: false,
                reverseDisabled: true,
                sliderDisabled: false
            })
        } else if (this.props.processExecutedLine.length === this.state.step) {
            this.setState({
                pauseDisabled: true,
                playDisabled: true,
                nextDisabled: false,
                reverseDisabled: false,
                sliderDisabled: false
            });
        } else {
            this.setState({
                pauseDisabled: true,
                playDisabled: false,
                nextDisabled: false,
                reverseDisabled: false,
                sliderDisabled: false
            });
        }
        clearInterval(this.timerId);
    }

    reverseFunction() {
        if (this.state.step === 0) {
            this.setState(prev => ({
                step: prev.step - 1,
                nextDisabled: false,
                playDisabled: false,
                reverseDisabled: true
            }));
        } else {
            this.setState(prev => ({
                step: prev.step - 1,
                nextDisabled: false,
                playDisabled: false
            }));
        }
    }

    nextFunction() {
        if (this.props.processExecutedLine.length - 1 === this.state.step) {
            this.setState(prev => ({
                step: prev.step + 1,
                nextDisabled: true,
                reverseDisabled: false,
                playDisabled: true,
            }));
        } else {
            this.setState(prev => ({
                step: prev.step + 1,
                reverseDisabled: false
            }));
        }
    }

    sendStepToParent() {
        this.props.sendStep(this.state.step);
    }

    sendLineToParent() {
        this.props.sendLine(this.state.line);
    }

    componentDidUpdate(prevProps, prevState) {
        //scrolling
        if (this.state.line !== prevState.line) {
            var div = document.getElementById('children1');
            if (this.state.line > prevState.line) {
                this.props.scrollInStep.forEach(step => {
                    if (this.state.step === step) {
                        div.scrollBy(0, 200);
                    }
                });
            } else if (this.state.line < prevState.line) {
                this.props.scrollInStep.forEach(step => {
                    if (this.state.step === step - 1) {
                        div.scrollBy(0, -200);
                    }
                });
            }
        }

        //logic of button, step, and passing info to parent
        if (prevState.step !== this.state.step) {
            if (prevState.step > this.state.step) {
                if (this.state.step === this.props.processExecutedLine.length - 1) {
                    this.setState({
                        nextDisabled: false,
                        playDisabled: false,
                    });
                }
            }
            if (this.state.step === -1) {
                clearInterval(this.timerId);
                this.setState({
                    reverseDisabled: true,
                    nextDisabled: false,
                    playDisabled: false,
                    sliderDisabled: false
                });
            }
            if (this.state.step === 0) {
                this.setState({
                    reverseDisabled: false
                });
            }
            if (this.state.step === this.props.processExecutedLine.length - 1 && !this.timerId) {
                this.setState({
                    nextDisabled: false,
                    playDisabled: false,
                });
            }
            if (this.state.step === this.props.processExecutedLine.length) {
                this.setState({
                    nextDisabled: true,
                    playDisabled: true,
                });
            }
            this.setState({
                line: this.props.processExecutedLine[this.state.step] - 1
            }, () => {
                this.sendLineToParent();
                this.sendStepToParent();
            });
            if (this.props.processExecutedLine.length === this.state.step) {
                clearInterval(this.timerId);
                this.setState({
                    pauseDisabled: true
                });
            }


            if (this.props.processExecutedLine.length === this.state.step) {
                this.setState({
                    reverseDisabled: false
                });
            }
        }
        if (prevProps.step !== this.props.step) {
            this.setState({
                line: this.props.line,
                step: this.props.step,
            });
        }
    }

    handleChangeState(event) {
        if (event !== this.state.step) {
            this.setState({ step: parseInt(event.target.value) });
        }
    }

    handleChangeSpeed(event) {
        if (parseInt(event.target.value) === 3000)
            this.setState({ speed: 1000 });
        if (parseInt(event.target.value) === 2000)
            this.setState({ speed: 2000 });
        if (parseInt(event.target.value) === 1000)
            this.setState({ speed: 3000 });
    }

    render() {
        let speed
        let start
        let number
        if (this.state.speed === 1000) {
            speed = "Fast"
        } else if (this.state.speed === 2000) {
            speed = "Normal"
        } else if (this.state.speed === 3000) {
            speed = "Slow"
        }
        if (this.state.step === -1) {
            this.start = "Start"
            this.number = "Drag to Start"
        } else {
            this.start = "Next"
            this.number = this.state.step
        }
        let max = this.props.processExecutedLine.length
        return (
            <div>
                <div>
                    <input
                        className="range"
                        type="range"
                        min="-1" max={max}
                        value={this.state.step}
                        onChange={this.handleChangeState.bind(this)}
                        step="1"
                        disabled={this.state.sliderDisabled} />
                    {this.number}
                </div>
                <div className="containerCode">
                    <button className="btn btn-primary buttonContainer" onClick={this.playFunction} disabled={this.state.playDisabled}><i className="fa fa-play"></i> Play</button>
                    <button className="btn btn-dark buttonContainer" onClick={this.stopFunction}><i className="fa fa-stop"></i> Stop</button>
                    <button className="btn btn-danger buttonContainer" onClick={this.pauseFunction} disabled={this.state.pauseDisabled}><i className="fa fa-pause"></i> Pause</button>
                    <button className="btn btn-primary buttonContainer" onClick={this.reverseFunction} disabled={this.state.reverseDisabled}><i className="fa fa-backward"></i> Previous</button>
                    <button className="btn btn-primary buttonContainer" onClick={this.nextFunction} disabled={this.state.nextDisabled}><i className="fa fa-forward"></i> {this.start}</button>

                    <input
                        className="range"
                        style={{ width: "100px" }}
                        type="range"
                        min="1000" max="3000"
                        onChange={this.handleChangeSpeed.bind(this)}
                        step="1000"
                        disabled={this.state.sliderDisabled} />
                    <div style={{ marginTop: "5px" }}>
                        {speed}
                    </div>
                </div>
                <div id='children1'>
                    {this.state.step === this.props.processExecutedLine.length ? (
                        <ol id="nav">
                            {this.props.processCode.map((item, index) => (// last REMOVE previous LINE
                                <li style={index === this.state.line ? this.liStyle : null}>{item}</li>
                            ))}
                        </ol>
                    ) : (
                            <ol id="nav">
                                {this.props.processCode.map((item, index) => (// NOT last 
                                    <li style={index === this.state.line ? this.liStyle : index === this.props.processExecutedLine[this.state.step - 1] - 1 ? this.before : null}>{item}</li>
                                ))}
                            </ol>
                        )}
                </div>
            </div>
        );
    }
}


export default ControlCode;