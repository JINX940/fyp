import React from "react";
import mermaid from "mermaid"

import "./Style.css"

class Graph extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            graphNumber: 0
        }
    }

    componentDidMount() {
        var config = {
            startOnLoad: true,
            flowchart: {
                useMaxWidth: true,
                htmlLabels: true,
                rankSpacing: 30,
            },
            securityLevel: 'loose',
        };

        mermaid.initialize(config);
    }

    forLoopSpecial(graphColor) {
        for (var x = 0; x < this.props.loop.length; x++) {
            debugger
            if (this.props.step === this.props.loop[x]) {
                graphColor = "\nstyle " + this.props.styleLoop[x] + " fill:#c3e3df"
            }
        }
        return graphColor;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        var graph = document.getElementById('graph');
        var graphColor = ""

        if (prevProps.step !== this.props.step) {
            if (this.props.step === -1) {
                graph.style.visibility = "hidden"
            }

            for (var l = 0; l < this.props.lineHighlight.length; l++) {
                var graphId = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                    'w', 'x', 'y', 'z'];

                if (this.props.lineHighlight[l] === this.props.line + 1) {
                    graphColor = "\nstyle " + graphId[l] + " fill:#c3e3df"
                    if (this.props.specialLine === this.props.line + 1) {
                        graphColor = this.forLoopSpecial(graphColor);
                    }
                }

            }
        }
        if (prevProps.step < this.props.step) {
            var LastGraph = this.props.graphDefinition.length - 1;
            var renderGraph = this.props.graphDefinition[LastGraph] + graphColor;
            graph.style.visibility = "visible"
            mermaid.render('theGraph', renderGraph, function (svgCode) {
                graph.innerHTML = svgCode
            })
        } else if (prevProps.step > this.props.step) {
            var LastGraph = this.props.graphDefinition.length - 1;
            var renderGraph = this.props.graphDefinition[LastGraph] + graphColor;
            graph.style.visibility = "visible"
            mermaid.render('theGraph', renderGraph, function (svgCode) {
                graph.innerHTML = svgCode
            })
        }
    }

    render() {
        return (
            <div>
                <div id="graph" />
            </div>
        );
    }
}

export default Graph;