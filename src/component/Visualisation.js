import React from "react";

import ControlCode from "./ControlCode";
import Variables from "./Variables";
import Graph from "./Graph"

import fordata from "../data/forloop.json"
import ifdata from "../data/ifdata.json"
import ifelsedata from "../data/ifelsedata.json"
import nestedifdata from "../data/nestedif.json"
import normaldata from "../data/normaldata.json"
import switchdata from "../data/switch.json"
import switchwbdata from "../data/switchwb.json"
import whiledata from "../data/whileloop.json"
import dowhiledata from "../data/dowhileloop.json"

// import { useParams } from "react-router-dom";

//variable data key(loop) follow step
//variable data key(lines,until) follow line

import "./Style.css"


class Visualisation extends React.Component {

    ControlCode = ifdata.data.ControlCode
    Variables = ifdata.data.Variables
    Graph = ifdata.data.Graph
    arrayGraph = this.Graph.graph
    topic = ""

    variables = this.Variables.variable

    constructor(props) {
        super(props)
        this.state = {
            step: -1,
            line: -1,
            key: "",
            graphDefinition: [],
            styleStep: {
                1: null,
                2: null,
                3: null,
                4: null
            },
            lineProcess: 0,
            fields: {
                x: '',
                y: '',
                z: '',
                i: ''
            },
            value: {
                x: [],
                y: [],
                z: [],
                i: []
            },
            stepUntil: {
                x: [],
                y: [],
                z: [],
                i: []
            },
            styleValue: {
                x: [],
                y: [],
                z: [],
                i: []
            },
            executedLine: [],
            scrollInStep: [],
            //for input
            processExecutedLine: [],
            processValue: [],
            processGraph: [],
            processCode: [],
            lineHighlight: [],
            specialLine: '',
            loop: [],
            styleLoop: []
        }
        this.getStep = this.getStep.bind(this);
        this.getLine = this.getLine.bind(this);
        this.getField = this.getField.bind(this);
        this.changeTopic = this.changeTopic.bind(this);

        this.processData = this.processData.bind(this);
        this.processExecutedLine = this.processExecutedLine.bind(this);
        this.processCode = this.processCode.bind(this);
        this.processGraph = this.processGraph.bind(this);
    }

    getStep(val) { // get data from child (ControlCode)
        this.setState({
            step: parseInt(val)
        })
    }

    getLine(val) { // get data from child (ControlCode)
        this.setState({
            line: parseInt(val)
        })
    }

    getField(val) { // get data from child (Variables)
        this.setState({
            fields: val
        }, () => {
            this.processData();
        })
    }

    changeTopic() { //Old Version
        var arrayFlow = [];
        var array = [];
        var string;

        switch (this.props.idFromParent) {
            case "for":
                this.setState({
                    key: 0
                })
                this.topic = fordata;
                break;

            case "if":
                this.setState({
                    key: 1
                })
                this.topic = ifdata;
                break;

            case "ifelse":
                this.setState({
                    key: 2
                })
                this.topic = ifelsedata;
                break;

            case "nestedif":
                this.setState({
                    key: 3
                })
                this.topic = nestedifdata;
                break;

            case "switch":
                this.setState({
                    key: 4
                })
                this.topic = switchdata;
                break;

            case "switchwb":
                this.setState({
                    key: 5
                })
                this.topic = switchwbdata;
                break;

            case "while":
                this.setState({
                    key: 6
                })
                this.topic = whiledata;
                break;

            case "dowhile":
                this.setState({
                    key: 7
                })
                this.topic = dowhiledata;
                break;

        }

        this.ControlCode = this.topic.data.ControlCode
        this.Graph = this.topic.data.Graph
        this.Variables = this.topic.data.Variables
        this.arrayGraph = this.Graph.graph
        var executedLine = []
        var styleVariableStep = []
        var styleVariableStep2 = []
        var styleVariableStep3 = []
        var styleVariableStep4 = []
        var scrollInStep = []

        for (var o = 0; o < this.ControlCode.executedLineWithStyle.length; o++) {

            if (typeof this.ControlCode.executedLineWithStyle[o] === "number") {
                executedLine.push(this.ControlCode.executedLineWithStyle[o])
            } else {
                var string = this.ControlCode.executedLineWithStyle[o]
                var num = string.match(/\d+/g)
                var letr = string.match(/[a-zA-Z]+/g);
                executedLine.push(num)

                if (letr[0].includes("a")) {
                    styleVariableStep.push(o)
                }
                if (letr[0].includes("b")) {
                    styleVariableStep2.push(o)
                }
                if (letr[0].includes("c")) {
                    styleVariableStep3.push(o)
                }
                if (letr[0].includes("d")) {
                    styleVariableStep4.push(o)
                }
                if (letr[0].includes("s")) {
                    scrollInStep.push(o)
                }
            }
        }
        for (var i = 0; i < this.arrayGraph.length; i++) { // to make graphDefinition into array.
            array.push(this.arrayGraph[i])
            string = array.join()
            var replace = string.split(",").join('\n')
            arrayFlow.push(replace);
        }

        let fields = this.state.fields
        for (var index = 0; index < this.Variables.variable.length; index++) {
            fields[this.Variables.variable[index].name] = this.Variables.variable[index].startingValue[0];//first value of variable
        }

        this.setState({
            graphDefinition: arrayFlow,
            styleStep: {
                0: styleVariableStep,
                1: styleVariableStep2,
                2: styleVariableStep3,
                3: styleVariableStep4,
            },
            executedLine: executedLine,
            scrollInStep: scrollInStep,
            processExecutedLine: executedLine,
            fields: fields,
            value: {
                x: [],
                y: [],
                z: [],
                i: []
            },
            stepUntil: {
                x: [],
                y: [],
                z: [],
                i: []
            },
            processGraph: this.arrayGraph,
            processCode: this.ControlCode.code,
            lineHighlight: [],
            specialLine: '',
            loop: [],
            styleLoop: []
        }, () => {
            this.processData()
        })
    }

    pushStyleValue(programCode, styleValue, numberOfStep) {
        var variables = programCode.match(/[a-zA-Z]+/g);
        variables.forEach(variable => {
            for (var key in styleValue) {
                if (key === variable) {
                    styleValue[key].push(numberOfStep);
                }
            }
        });
        return styleValue
    }
    switchChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x) {
        executedLine.push(line + 1);
        executedLine.push(line + 2);
        line += 2;
        stepUntil['x'].push(20);
        numberOfStep +=1;
        styleValue['x'].push(numberOfStep);
        numberOfStep +=1;
        debugger
        for (var q = line; q < this.state.processCode.length; q++) {
            if (this.state.processCode[q].includes("default")) {
                executedLine.push(q + 1);
                executedLine.push(q + 2);
                executedLine.push(q + 3);
                break
            }
            if (this.state.processCode[q].includes("case")) {
                var num = this.state.processCode[q].match(/\d+/g)
                if (x === parseInt(num)) { 
                    numberOfStep +=1;
                    styleValue['x'].push(numberOfStep);
                    executedLine.push(q + 1);
                    executedLine.push(q + 2);
                    executedLine.push(q + 3);
                    q = 20;
                } else if (!this.state.processCode[q].includes("default")) {
                    numberOfStep +=1;
                    styleValue['x'].push(numberOfStep);
                    executedLine.push(q + 1);
                    q += 3;
                }
                if (this.state.processCode[q].includes("}")) {
                    executedLine.push(q + 1);
                    break;
                }
            }
        }
        return [line, executedLine, numberOfStep, value, stepUntil, styleValue, x]
    }

    forChecking(line, executedLine, numberOfStep, value, stepUntil, loop, styleLoop, styleValue, x, y, z, v, i) {
        numberOfStep += 1;
        var string = this.state.processCode[line].substring(this.state.processCode[line].indexOf('int') + 3, this.state.processCode[line].indexOf(';'))
        var letr = string.match(/[a-zA-Z]+/g); // for getting i
        value[letr[0]].push(eval(this.state.processCode[line].substring(this.state.processCode[line].indexOf('int') + 3, this.state.processCode[line].indexOf(';')))); // instantiate

        executedLine.push(line + 1); // for instantiate i

        var condition = (this.state.processCode[line].substring(this.state.processCode[line].indexOf(';') + 1, this.state.processCode[line].lastIndexOf(';')));
        var increment = (this.state.processCode[line].substring(this.state.processCode[line].lastIndexOf(';') + 1, this.state.processCode[line].lastIndexOf(')')));
        executedLine.push(line + 1); //this is for checking condition
        eval(increment) // NTD SPECIAL I
        numberOfStep += 1;
        styleValue = this.pushStyleValue(condition, styleValue, numberOfStep);
        loop.push(numberOfStep);
        if (eval(condition)) {
            line += 1;
            for (var q = line; q < this.state.processCode.length; q++) {
                if (eval(condition)) {
                    if (!this.state.processCode[q].includes("}")) {
                        numberOfStep += 1;
                        if (this.state.processCode[q].includes("=")) {
                            styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                            var letr = this.state.processCode[q].match(/[a-zA-Z]+/g);
                            value[letr[0]].push(eval(this.state.processCode[q]));
                            stepUntil[letr[0]].push(numberOfStep);
                        }
                        executedLine.push(q + 1);
                    } else {
                        numberOfStep += 2;
                        styleValue['i'].push(numberOfStep)
                        loop.push(numberOfStep);
                        executedLine.push(q + 1);
                        executedLine.push(line); // increment in line for, so push line
                        var aString = this.state.processCode[line - 1].substring(this.state.processCode[line - 1].indexOf('int') + 3, this.state.processCode[line - 1].indexOf(';')) // get int i
                        var letr = aString.match(/[a-zA-Z]+/g); // for getting i
                        stepUntil[letr[0]].push(numberOfStep);
                        value[letr[0]].push(eval(increment));
                        styleLoop.push(this.Graph.specialNode[0]);
                        styleLoop.push(this.Graph.specialNode[1]);
                        styleLoop.push(this.Graph.specialNode[0]);
                        styleLoop.push(this.Graph.specialNode[1]);
                        if (eval(condition)) {
                            numberOfStep += 1;
                            styleValue = this.pushStyleValue(condition, styleValue, numberOfStep);
                            loop.push(numberOfStep);
                            q = line - 1; // line currently in {
                            executedLine.push(q + 1);
                        } else {
                            numberOfStep += 1;
                            styleValue = this.pushStyleValue(condition, styleValue, numberOfStep);
                            loop.push(numberOfStep);
                            var lastLine = q + 1// for skipping the line between for loop
                            q = line - 1; // currently line is in {
                            line = lastLine
                            executedLine.push(q + 1);
                            q = this.state.processCode.length;
                        }
                    }
                } else {
                    // not entering for loop, skip push
                    if (this.state.processCode[q].includes("}")) {//skip
                        line = q + 1;
                        q = this.state.processCode.length;
                    }
                }
            }
        } else { // skip 
            for (var q = line; q < this.state.processCode.length; q++) {
                if (this.state.processCode[q].includes("}")) {//skip
                    line = q + 1;
                    q = this.state.processCode.length;
                }
            }
        }
        return [line, executedLine, numberOfStep, value, stepUntil, loop, styleLoop, styleValue, x, y, z, v, i];
    }

    ifElseChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i) {

        executedLine.push(line + 1);
        var condition = this.state.processCode[line].substring(this.state.processCode[line].indexOf('(') + 1, this.state.processCode[line].indexOf(')'));
        line += 1;
        numberOfStep += 1;

        styleValue = this.pushStyleValue(this.state.processCode[line - 1], styleValue, numberOfStep);
        var letr = this.state.processCode[line].match(/[a-zA-Z]+/g);
        if (eval(condition)) { // True If condition, push the line
            for (var q = line; q < this.state.processCode.length; q++) {
                if (!this.state.processCode[q].includes("}")) {
                    debugger
                    executedLine.push(q + 1);
                    numberOfStep += 1;
                    if (this.state.processCode[q].includes('if')) {
                        [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i] = this.ifElseChecking(q, executedLine, numberOfStep - 1, value, stepUntil, styleValue, x, y, z, v, i)
                        q = line;
                        if (this.state.processCode[q].includes("}")) {
                            numberOfStep += 1;
                            executedLine.push(q + 1);
                            line = q + 1;
                            q = this.state.processCode.length;
                        }
                    }
                    if (q !== this.state.processCode.length) {
                        debugger
                        if (this.state.processCode[q].includes("=")) {
                            executedLine.push(q + 1);
                            styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                            var letr = this.state.processCode[q].match(/[a-zA-Z]+/g);
                            value[letr[0]].push(eval(this.state.processCode[q]));
                            stepUntil[letr[0]].push(numberOfStep);
                        }
                    }
                } else if (q !== this.state.processCode.length) {
                    numberOfStep += 1;
                    executedLine.push(q + 1);
                    line = q + 1;
                    if (this.state.processCode[line].includes("else")) {//skip else , if condition is true
                        for (var k = line; k < this.state.processCode.length; k++) {
                            if (this.state.processCode[k].includes("}")) {
                                line = k + 1;
                                k = this.state.processCode.length;
                            }
                        }
                    }
                    q = this.state.processCode.length;
                }
            }
        } else {
            for (var q = line; q < this.state.processCode.length; q++) {
                if (this.state.processCode[q].includes("}")) { //skip the if line
                    line = q + 1;
                    q = this.state.processCode.length;
                }
            }
            if (this.state.processCode[line].includes("else")) {// If have else ,True Else condition, push the line
                for (var q = line; q < this.state.processCode.length; q++) {
                    if (!this.state.processCode[q].includes("}")) {
                        numberOfStep += 1;
                        if (this.state.processCode[q].includes("=")) {
                            styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                            var letr = this.state.processCode[q].match(/[a-zA-Z]+/g);
                            value[letr[0]].push(eval(this.state.processCode[q]));
                            stepUntil[letr[0]].push(numberOfStep);
                        }
                        executedLine.push(q + 1);
                    } else {
                        numberOfStep += 1;
                        executedLine.push(q + 1);
                        line = q + 1;
                        q = this.state.processCode.length;
                    }
                }
            }
        }
        return [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i];
    }

    doWhileChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i) {
        executedLine.push(line + 1);
        line += 1; // line is in { , loop back here
        numberOfStep += 1;
        var loopLine = line;
        for (var q = loopLine; q < this.state.processCode.length; q++) {
            if (!this.state.processCode[q].includes("}")) {
                numberOfStep += 1;
                if (this.state.processCode[q].includes("=")) {
                    styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                    var letr = this.state.processCode[q].match(/[a-zA-Z]+/g);
                    value[letr[0]].push(eval(this.state.processCode[q]));
                    stepUntil[letr[0]].push(numberOfStep);
                }
                executedLine.push(q + 1);

            } else {
                executedLine.push(q + 1); //push while line
                numberOfStep += 1;
                styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                if (this.state.processCode[q].includes("while")) {
                    var condition = this.state.processCode[q].substring(this.state.processCode[q].indexOf('(') + 1, this.state.processCode[q].indexOf(')'));
                    if (eval(condition)) {
                        q = loopLine - 1;
                    } else { //exit do while loop
                        line = q + 1;
                        q = this.state.processCode.length;
                    }
                }
            }
        }
        return [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i];
    }

    whileChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i) {
        var condition = this.state.processCode[line].substring(this.state.processCode[line].indexOf('(') + 1, this.state.processCode[line].indexOf(')'));
        executedLine.push(line + 1);
        numberOfStep += 1;
        styleValue = this.pushStyleValue(condition, styleValue, numberOfStep);
        if (eval(condition)) {
            line += 1;
            for (var q = line; q < this.state.processCode.length; q++) {
                if (!this.state.processCode[q].includes("}")) {
                    numberOfStep += 1;
                    if (this.state.processCode[q].includes("=")) {
                        styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                        var letr = this.state.processCode[q].match(/[a-zA-Z]+/g);
                        value[letr[0]].push(eval(this.state.processCode[q]));
                        stepUntil[letr[0]].push(numberOfStep);
                    }
                    executedLine.push(q + 1);
                } else {
                    executedLine.push(q + 1);
                    numberOfStep += 1;

                    if (eval(condition)) { //check condition
                        numberOfStep += 1;
                        q = line - 1; // current line is in while
                        styleValue = this.pushStyleValue(this.state.processCode[q], styleValue, numberOfStep);
                        executedLine.push(line); //push while line
                    } else {
                        numberOfStep += 1;
                        executedLine.push(line); //push while line
                        styleValue = this.pushStyleValue(this.state.processCode[line - 1], styleValue, numberOfStep);
                        line = q;
                        q = this.state.processCode.length;
                    }
                }
            }
        } else {
            for (var q = line; q < this.state.processCode.length; q++) {
                if (this.state.processCode[q].includes("}")) {//skip
                    line = q + 1;
                    q = this.state.processCode.length;
                }
            }
        }
        return [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i];
    }

    processCode() { //OBJECTIVE CHANGE the code for before executed line
        var temporaryCode = this.state.processCode;
        for (var line = 0; line < temporaryCode.length; line++) { // skip for main and { line,
            if (temporaryCode[line].includes("int x = ")
                || temporaryCode[line].includes("int y = ")
                || temporaryCode[line].includes("int z = ")
                || temporaryCode[line].includes("int i = ")
                || temporaryCode[line].includes("bool")) {
                if (temporaryCode[line].includes("int i = ")) { // for loop
                    var string = temporaryCode[line].substring(temporaryCode[line].indexOf('(') + 1, temporaryCode[line].indexOf(';'))
                } else {
                    var string = temporaryCode[line]
                }
                var num = string.match(/\d+/g)
                var letr = string.match(/[a-zA-Z]+/g); // [0] = int [1] = x or y or z
                do {
                    var replaceDone = false;
                    for (var key in this.state.fields) {

                        if (key === letr[1]) {
                            if (temporaryCode[line].includes("bool")) {
                                var replace = temporaryCode[line].replace(letr[2], this.state.fields[key]);
                                temporaryCode[line] = replace
                                replaceDone = true;
                            } else {
                                var replace = temporaryCode[line].replace(num, this.state.fields[key]);
                                temporaryCode[line] = replace
                                replaceDone = true;
                            }

                        }
                    }
                } while (!replaceDone)

            }
        }
        this.setState({
            processCode: temporaryCode
        })
    }
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    //THIS PART IS FOR PROCESS EXECUTED LINE >>>>>>

    processExecutedLine() {
        // for passing to component
        var executedLine = [];
        var numberOfStep = -1;
        var loop = [];

        var value = { x: [], y: [], z: [], i: [] };
        var stepUntil = { x: [], y: [], z: [], i: [] };
        var styleValue = { x: [], y: [], z: [], i: [] }; //when condition or statement, style value
        //for variable
        var x
        var v
        var y
        var z

        // for loop
        var i

        // for line that need to highlight Graph
        var lineHighlight = []
        var specialLine // set this for for loop i 
        var styleLoop = []; //push special node; e,g,e,g,e,g.....

        var line = 0

        for (line = 0; line < this.state.processCode.length; line++) {
            if (line === 0) { // push main and {
                if (this.state.processCode[line].includes("main()")) {
                    executedLine.push(line + 1);
                    numberOfStep += 1;
                    if (this.state.processCode[line + 1].includes("{")) {
                        numberOfStep += 1;
                        executedLine.push(line + 2);
                    }
                }
                line += 1;

            }

            if (this.state.processCode[line].includes("int") && !this.state.processCode[line].includes("for")) { // Instantiate variable INT
                executedLine.push(line + 1);
                if (this.state.processCode[line].indexOf('int')) {
                    var string = this.state.processCode[line].substring(this.state.processCode[line].indexOf('int') + 4, this.state.processCode[line].length);
                } else if (this.state.processCode[line].indexOf('bool')) {
                    var string = this.state.processCode[line].substring(this.state.processCode[line].indexOf('bool') + 4, this.state.processCode[line].length);
                }

                var num = string.match(/\d+/g);
                var letr = string.match(/[a-zA-Z]+/g);
                if (num !== null) {
                    value[letr[0]].push(parseInt(num[0]));
                } else {
                    value[letr[1]].push(letr[2]);
                }
                eval(string);
                numberOfStep += 1;
            }

            if (this.state.processCode[line].includes("bool")) { // Instantiate variable bool
                executedLine.push(line + 1);
                if (this.state.processCode[line].indexOf('bool')) {
                    var string = this.state.processCode[line].substring(this.state.processCode[line].indexOf('bool') + 4, this.state.processCode[line].length);
                }
                var letr = string.match(/[a-zA-Z]+/g);
                value[letr[0]].push(letr[1]);
                eval(string);
                numberOfStep += 1;
            }

            if (this.state.processCode[line].includes("if")) {//if and if else
                [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i] = this.ifElseChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i);
            }

            if (this.state.processCode[line].includes("for")) {//for loop
                [line, executedLine, numberOfStep, value, stepUntil, loop, styleLoop, styleValue, x, y, z, v, i] = this.forChecking(line, executedLine, numberOfStep, value, stepUntil, loop, styleLoop, styleValue, x, y, z, v, i);
            }

            if (this.state.processCode[line].includes("do")) {
                [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i] = this.doWhileChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i);
            }

            if (this.state.processCode[line].includes("while")) {
                [line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i] = this.whileChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x, y, z, v, i);
            }
            if (this.state.processCode[line].includes("switch")) {
                [line, executedLine, numberOfStep, value, stepUntil, styleValue, x] = this.switchChecking(line, executedLine, numberOfStep, value, stepUntil, styleValue, x);
            }

            if (this.state.processCode[line].includes("return 0;")) { // push return 0 and }
                executedLine.push(line + 1);
                numberOfStep += 1;
                if (this.state.processCode[line + 1].includes("}")) {
                    executedLine.push(line + 2);
                    line += 1;
                    numberOfStep += 1;
                }
            }
        }

        for (var t = 0; t < this.state.processCode.length; t++) { // for line that need to hightlight Graph
            if (this.state.processCode[t].includes("int") ||
                this.state.processCode[t].includes("if") ||
                this.state.processCode[t].includes("for") ||
                this.state.processCode[t].includes("main") ||
                this.state.processCode[t].includes("=") ||
                this.state.processCode[t].includes("cout") ||
                this.state.processCode[t].includes("switch") ||
                this.state.processCode[t].includes("case") ||
                this.state.processCode[t].includes("default:") ||
                this.state.processCode[t].includes("return")) {
                if (this.state.processCode[t].includes("default:")) {
                    lineHighlight.push(t + 1);
                    t += 2;// skip because one graph only
                } else if (this.state.processCode[t].includes("return")) {
                    lineHighlight.push(t + 1);
                    lineHighlight.push(t + 2); // do this for }
                } else if (this.state.processCode[t].includes("for")) {
                    var skipLine = 0
                    lineHighlight.push(t + 1);
                    t += 1;
                    lineHighlight.push('nothing');
                    for (var l = t; l < this.state.processCode.length; l++) {
                        if (this.state.processCode[l].includes("=")) {
                            lineHighlight.push(l + 1);
                            skipLine += 1;
                        }
                        if (this.state.processCode[l].includes("}")) {
                            break;
                        }
                    }
                    lineHighlight.push('nothing');
                    specialLine = t
                    t += skipLine;
                } else {
                    lineHighlight.push(t + 1);
                }
            }
        }

        console.log(stepUntil['x'])
        for (var key in stepUntil) {
            if (key === 'i') { //for loop i
                var length = stepUntil[key].length
                var lastStepForI = stepUntil[key][length - 1]
                stepUntil[key].push(lastStepForI + 1);
            } else {
                stepUntil[key].push(numberOfStep + 1);
            }
        }
        var removeDuplicateExecutedLine = executedLine.filter((item, index) => executedLine.indexOf(item) === index);
        console.log(removeDuplicateExecutedLine);
        this.setState({
            processExecutedLine: removeDuplicateExecutedLine,
            stepUntil: stepUntil,
            value: value,
            lineHighlight: lineHighlight,
            specialLine: specialLine,
            loop: loop,
            styleLoop: styleLoop,
            styleValue: styleValue
        })
    }
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //THIS part is for PROCESS CODE

    processGraph() {
        var temporaryGraph = this.state.processGraph;
        for (var line = 0; line < temporaryGraph.length; line++) { // skip for main and { line,
            if (temporaryGraph[line].includes("int x = ")
                || temporaryGraph[line].includes("int y = ")
                || temporaryGraph[line].includes("int z = ")) {
                var string = temporaryGraph[line]
                var num = string.match(/\d+/g)
                var letr = string.match(/[a-zA-Z]+/g);
                //find the string after = and change
                temporaryGraph[line] = string.replace(num, this.state.fields[letr[3]]);// [0] = graphid [1] = graph id, [2] x,y or z, [3] = value
            }
        }
        this.setState({
            processGraph: temporaryGraph,
        })
    }

    processData() {
        this.processCode()
        this.processExecutedLine()
        this.processGraph()
    }

    componentDidMount() {
        this.changeTopic();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.idFromParent !== this.props.idFromParent) {
            this.changeTopic();
        }
    }

    render() {
        return (
            <div className='mainContainer'>
                <div className='container1'>
                    <ControlCode
                        processCode={this.state.processCode}
                        step={this.state.step}
                        processExecutedLine={this.state.processExecutedLine}
                        scrollInStep={this.state.scrollInStep}
                        line={this.state.line}
                        sendStep={this.getStep}
                        sendLine={this.getLine}>
                    </ControlCode>
                    <Variables
                        key={this.state.key}
                        Variables={this.Variables}
                        line={this.state.line}
                        step={this.state.step}
                        styleStep={this.state.styleStep}
                        value={this.state.value}
                        styleValue={this.state.styleValue}
                        stepUntil={this.state.stepUntil}
                        sendFields={this.getField}>
                    </Variables>
                </div>
                <div className='container2'>
                    <Graph
                        key={this.state.key}
                        lineHighlight={this.state.lineHighlight}
                        specialLine={this.state.specialLine}
                        loop={this.state.loop}
                        styleLoop={this.state.styleLoop}
                        Graph={this.Graph}
                        line={this.state.line}
                        step={this.state.step}
                        graphDefinition={this.state.graphDefinition}>
                    </Graph>
                </div>
            </div>
        )
    }
}


export default Visualisation;