import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import "./Style.css"
import About from "./About"
import Basic from "./Basic"
import Child from "./Child"

class SideBarNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "",
      toggle: false,
      isURL: false
    }
    this.closeNav = this.closeNav.bind(this);
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    this.setState({
      isURL: true
    })
  }
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }

  componentDidMount() {
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function () {

        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
          dropdownContent.style.display = "none";
        } else {
          dropdownContent.style.display = "block";
        }
      });
    }
  }

  render() {
    return (
      <Router>
        <nav className="navbar navbar-dark bg-dark">
          <div id="mySidenav" className="sidenav" >
            <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
            <Link to={"/"} onClick={this.closeNav}>About</Link>
            {/* <Link to={"/basic"} onClick={this.closeNav}>Basic</Link> */}
            <Link to={"/if"} onClick={this.closeNav}>If</Link>
            <Link to={"/ifelse"} onClick={this.closeNav}>If Else</Link>
            <Link to={"/nestedif"} onClick={this.closeNav}>Nested If</Link>
            <Link to={"/switch"} onClick={this.closeNav}>Switch</Link>
            <button className="dropdown-btn">Loop
    <i className="fa fa-caret-down"></i>
            </button>
            <div className="dropdown-container">
              <Link to={"/dowhile"} onClick={this.closeNav}>Do While</Link>
              <Link to={"/for"} onClick={this.closeNav}>For</Link>
              <Link to={"/while"} onClick={this.closeNav}>While</Link>
            </div>

          </div>
          <span className="navbar-brand" style={{ cursor: "pointer" }} onClick={this.openNav}>&#9776; SideBar</span>
        </nav>
        <Switch>
          <Route exact path='/' component={About} />
          {/* <Route path='/basic' component={Basic} /> */}
          <Route path='/:id' children={<Child></Child>} />
        </Switch>

      </Router>
    );
  }
}

export default SideBarNavigation;