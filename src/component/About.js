import React from "react";

import "./Style.css"

class About extends React.Component {

    state = {
        code: [
            "int main()",
                "{",
                "    int x = 6;",
                "    ",
                "        if (x > 5)",
                "        {",
                "            cout << \"x is larger than 5\"<< endl;",
                "        }",
                "        else",
                "        {",
                "            cout << \"x is smaller that 5\"<< endl;",
                "        }",
                "        ",
                "    return 0;",
                "}"
        ],
        testing: null,
    }

    testing() {
        console.log("hi")
    }

    forChecking(line, executedLine, x, y, z, v, i) {
        eval(this.state.code[line].substring(this.state.code[line].indexOf('int') + 3, this.state.code[line].indexOf(';'))); // instantiate
        executedLine.push(line + 1); // for instantiate i
        var condition = (this.state.code[line].substring(this.state.code[line].indexOf(';') + 1, this.state.code[line].lastIndexOf(';')));
        var increment = (this.state.code[line].substring(this.state.code[line].lastIndexOf(';') + 1, this.state.code[line].lastIndexOf(')')));
        executedLine.push(line + 1); //this is for checking condition
        if (eval(condition)) {
            line += 1;
            for (var q = line; q < this.state.code.length; q++) {
                if (eval(condition)) {
                    if (!this.state.code[q].includes("}")) {
                        if (this.state.code[q].includes("=")) {
                            eval(this.state.code[q]);
                        }
                        executedLine.push(q + 1);
                    } else {
                        executedLine.push(q + 1);
                        eval(increment);
                        executedLine.push(line); // increment in line for, so push line
                        if (eval(condition)) {
                            q = line - 1; // line currently in {
                            executedLine.push(q + 1);
                        } else {
                            var lastLine = q + 1// for skipping the line between for loop
                            q = line - 1; // currently line is in {
                            line = lastLine
                            executedLine.push(q + 1);
                            q = this.state.code.length;
                        }
                    }
                } else {
                    // not entering for loop, skip push
                    if (this.state.code[q].includes("}")) {//skip
                        line = q + 1;
                        q = this.state.code.length;
                    }
                }
            }
        } else { // skip 
            for (var q = line; q < this.state.code.length; q++) {
                if (this.state.code[q].includes("}")) {//skip
                    line = q + 1;
                    q = this.state.code.length;
                }
            }
        }
        return [line, executedLine, x, y, z, v, i];
    }

    ifElseChecking(line, executedLine, x, y, z, v, i) {
        executedLine.push(line + 1);
        var condition = this.state.code[line].substring(this.state.code[line].indexOf('(') + 1, this.state.code[line].indexOf(')'));
        line += 1;
        if (eval(condition)) { // True If condition, push the line
            for (var q = line; q < this.state.code.length; q++) {
                if (!this.state.code[q].includes("}")) {
                    if (this.state.code[q].includes("=")) {
                        eval(this.state.code[q]);
                    }
                    executedLine.push(q + 1);
                } else {
                    executedLine.push(q + 1);
                    line = q + 1;
                    if (this.state.code[line].includes("else")) {
                        for (var k = line; k < this.state.code.length; k++) {
                            if (this.state.code[k].includes("}")) {
                                line = k + 1;
                                k = this.state.code.length;
                            }
                        }
                    }
                    q = this.state.code.length;
                }
            }
        } else {
            for (var q = line; q < this.state.code.length; q++) {
                if (this.state.code[q].includes("}")) {//skip
                    line = q + 1;
                    q = this.state.code.length;
                }
            }
            if (this.state.code[line].includes("else")) {// If have else ,True Else condition, push the line
                for (var q = line; q < this.state.code.length; q++) {
                    if (!this.state.code[q].includes("}")) {
                        if (this.state.code[q].includes("=")) {
                            eval(this.state.code[q]);
                        }
                        executedLine.push(q + 1);
                    } else {
                        executedLine.push(q + 1);
                        line = q + 1;
                        q = this.state.code.length;
                    }
                }
            }
        }
        return [line, executedLine, x, y, z, v, i];
    }

    doWhileChecking(line, executedLine, x, y, z, v, i) {
        executedLine.push(line + 1);
        line += 1; // line is in { , loop back here
        var loopLine = line;
        for (var q = loopLine; q < this.state.code.length; q++) {
            if (!this.state.code[q].includes("}")) {
                if (this.state.code[q].includes("=")) {
                    eval(this.state.code[q]);
                }
                executedLine.push(q + 1);
            } else {
                executedLine.push(q + 1); //push while line
                if (this.state.code[q].includes("while")) {
                    var condition = this.state.code[q].substring(this.state.code[q].indexOf('(') + 1, this.state.code[q].indexOf(')'));
                    if (eval(condition)) {
                        q = loopLine - 1;
                    } else { //exit do while loop
                        line = q + 1;
                        q = this.state.code.length;
                    }
                }
            }
        }
        return [line, executedLine, x, y, z, v, i];
    }

    whileChecking(line, executedLine, x, y, z, v, i) {
        var condition = this.state.code[line].substring(this.state.code[line].indexOf('(') + 1, this.state.code[line].indexOf(')'));
        executedLine.push(line + 1);
        if (eval(condition)) {
            line += 1;
            for (var q = line; q < this.state.code.length; q++) {
                if (!this.state.code[q].includes("}")) {
                    if (this.state.code[q].includes("=")) {
                        eval(this.state.code[q]);
                    }
                    executedLine.push(q + 1);
                } else {
                    executedLine.push(q + 1);
                    if (eval(condition)) { //check condition
                        q = line - 1; // current line is in while
                        executedLine.push(line); //push while line
                    } else {
                        executedLine.push(line); //push while line
                        line = q;
                        q = this.state.code.length;
                    }
                }
            }
        } else {
            for (var q = line; q < this.state.code.length; q++) {
                if (this.state.code[q].includes("}")) {//skip
                    line = q + 1;
                    q = this.state.code.length;
                }
            }
        }
        return [line, executedLine, x, y, z, v, i];
    }

    componentDidMount() {
        // for passing to component
        var executedLine = []
        eval("this.testing()")
        //for variable
        var x
        var v
        var y
        var z

        // for loop
        var i

        // for line that need to highlight Graph
        var lineHighlight = []

        var line = 0

        for (line = 0; line < this.state.code.length; line++) {
            if (line === 0) { // push main and {
                if (this.state.code[line].includes("main()")) {
                    executedLine.push(line + 1);
                    if (this.state.code[line + 1].includes("{")) {
                        executedLine.push(line + 2);
                    }
                }
                line += 1;
            }

            if (this.state.code[line].includes("int") && !this.state.code[line].includes("for")) { // Instantiate variable
                executedLine.push(line + 1);
                var string = this.state.code[line].substring(this.state.code[line].indexOf('int') + 4, this.state.code[line].length);
                eval(string);
            }

            if (this.state.code[line].includes("if")) {//if and if else
                [line, executedLine, x, y, z, v, i] = this.ifElseChecking(line, executedLine, x, y, z, v, i);
                console.log(line)
            }

            if (this.state.code[line].includes("for")) {//for loop
                [line, executedLine, x, y, z, v, i] = this.forChecking(line, executedLine, x, y, z, v, i);
                console.log(line)
            }

            if (this.state.code[line].includes("do")) {
                [line, executedLine, x, y, z, v, i] = this.doWhileChecking(line, executedLine, x, y, z, v, i);
                console.log(line)
            }

            if (this.state.code[line].includes("while")) {
                [line, executedLine, x, y, z, v, i] = this.whileChecking(line, executedLine, x, y, z, v, i);
                console.log(line)
            }

            if (this.state.code[line].includes("return 0;")) { // push return 0 and }
                executedLine.push(line + 1);
                if (this.state.code[line + 1].includes("}")) {
                    executedLine.push(line + 2);
                    line += 1;
                }
            }
        }

        for (var t = 0; t < this.state.code.length; t++) { // for line that need to hightlight Graph
            if (this.state.code[t].includes("int") ||
                this.state.code[t].includes("if") ||
                this.state.code[t].includes("for") ||
                this.state.code[t].includes("main") ||
                this.state.code[t].includes("=") ||
                this.state.code[t].includes("cout")||
                this.state.code[t].includes("return")) {
                if (this.state.code[t].includes("return")) {
                    lineHighlight.push(t + 1);
                    lineHighlight.push(t + 2); // do this for }
                } else {
                    lineHighlight.push(t + 1);
                }
            }
        }
        console.log(lineHighlight)

        this.setState({
            testing: executedLine
        })
    }

    render() {
        return (
            <div >
                {/* <h1>{this.state.testing}</h1> */}
            </div>
        );
    }
}

export default About;